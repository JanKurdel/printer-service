require 'dry-struct'
require 'dry-validation'

module PrinterService
  module Types
    include Dry::Types.module
  end

  class BaseModel < Dry::Struct
  end
end
