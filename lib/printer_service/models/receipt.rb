require_relative 'base_model'
require_relative 'line'

module PrinterService
  ReceiptSchema = Dry::Validation.Schema do
    required(:receipt_id).filled(:int?)
    required(:payments_total).filled(:int?)
    required(:price_total).filled(:int?)
  end

  class Receipt < BaseModel
    attribute :receipt_id, Types::Coercible::Int
    attribute :payments_total, Types::Coercible::Int
    attribute :price_total, Types::Coercible::Int

    attribute :lines, Types::Coercible::Array.member(Line)
  end
end
