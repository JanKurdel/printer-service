require 'json'
require 'rest-client'
require_relative 'models/receipt'
require_relative 'models/line'
require_relative '../extensions/hash'
#TODO: remove this quickfix
require 'bundler'
Bundler.setup
require 'fiscal_printer'

module PrinterService
  class HttpClient
    def initialize(host)
      @host = host
    end

    def authorize(username, password, sha1)
      response = RestClient.post(
        "#{@host}/api/v1/user/postLogin",
        username: username,
        password1: password,
        sha1: sha1
      )
      @cookies = response.cookies
    end

    def first_receipt
      response = RestClient.get("#{@host}/api/v1/printer/getFirstReceiptToPrint", cookies: @cookies)
      json = JSON.parse(response.body)
      json = convert_hash_keys(json)

      FiscalPrinter::ReceiptSchema.call(json[:response_data][:receipt] || {})
    end

    def update_receipt(receipt_id)
      RestClient.post(
        "#{@host}/api/v1/printer/changeReceiptAsPrinted",
        {
          receiptId: receipt_id,
          printDate: Time.now.to_i
        },
        cookies: @cookies
      )
    end

    def printer_error(receipt_id, user_id, error_code, error_message)
      RestClient.post(
        "#{@host}/api/v1/printer/postPrinterError",
        {
          receiptId: receipt_id,
          authorId: user_id,
          errorCode: error_code,
          errorDescription: error_message
        },
        cookies: @cookies
      )
    end
  end
end
