#TODO: remove this quickfix
require 'bundler'
Bundler.setup
require 'fiscal_printer'

module PrinterService
  class PrinterClient
    def initialize(port, baud_rate, data_bits)
      @serial = FiscalPrinter::SerialClient.new({ port: port, baud_rate: baud_rate })
    end

    def print_receipt(receipt)
      message = ::Thermal::Message::Trinit.new(
        items_count: receipt.lines.count
      )

      @serial.write(message.to_s)
      p message.to_s
      sleep(0.1)

      receipt.lines.each_with_index do |line, idx|
        send_line(line, idx)
      end

      message = Thermal::Message::Trend.new(
        payment_value: receipt.payments_total,
        total_value: receipt.price_total
      )

      p message.to_s
      @serial.write(message.to_s)
      sleep(0.1)
    end

    private

    def send_line(line, idx)
      message = Thermal::Message::Trline.new(
        line_number: idx + 1,
        discount: 0,
        discount_description: 0,
        name: line.description,
        quantity: line.count,
        price: line.price,
        total_price: line.total_price,
        ptu: 'A'
      )

      p message.to_s
      @serial.write(message.to_s)
      sleep(0.1)
    end
  end
end
