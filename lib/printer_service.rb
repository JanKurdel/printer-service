require_relative 'printer_service/http_client'
require_relative 'printer_service/printer_client'

require 'yaml'

module PrinterService
  class Configuration
    attr_accessor :host, :username, :password, :sha, :port, :baud_rate, :protocol
  end

  class << self
    attr_writer :configuration
  end

  def self.configuration
    @configuration ||= Configuration.new
  end

  def self.configure
    yield configuration
  end

  def self.start
    p 'Starting application...'

    http_client = HttpClient.new(configuration.host)

    p 'Authorizing client...'
    http_client.authorize(
      configuration.username,
      configuration.password,
      configuration.sha,
    )

    begin
      serial_client = FiscalPrinter::SerialClient.new(
        port: configuration.port,
        baud_rate: configuration.baud_rate
      )
    rescue FiscalPrinter::PrinterError => e
      result = http_client.printer_error(
        nil, nil, e.code, 'Error connecting to printer'.freeze
      )
      raise e
    end

    driver = FiscalPrinter::DriverFactory.create(configuration.protocol, serial_client)

    loop do
      result = http_client.first_receipt

      p result
      if result.success?
        p 'Sending receipt to the printer...'
        p result.output
        receipt = FiscalPrinter::Receipt.new(result.output)
        p receipt
        driver.print_receipt(receipt)
        #response = http_client.update_receipt(receipt.receipt_id)
        #p response.body
      end

      p 'Waiting for receipt...'
      sleep(5)
    end
  end
end


config = YAML.load_file("#{File.dirname(__FILE__)}/../config/config.yml")
PrinterService.configure do |c|
  c.host = config['host']
  c.username =  config['username']
  c.password = config['password']
  c.sha = config['sha']
  c.port = config['port']
  c.baud_rate = config['baud_rate']
  c.protocol = config['protocol']
end

PrinterService.start
